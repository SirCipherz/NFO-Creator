#!/usr/bin/python3
import cv2
from os import path, system
from decimal import Decimal
from math import floor
from sys import argv
import subprocess
import re

"""
Fonctions
"""

def TimeFormat(t):
    if t < 60:
        return str(t) + 's'
    elif t < 3600:
        return str(floor(t/60)) + 'min ' + str(floor((((t/60) - floor(t/60)) * 60))) + 's'

def FormatData(n):
    if n < 1024:
        return str(n) + 'o'
    elif n/1024 < 1024:
        return str(Decimal(n/1024).quantize(Decimal('.02'))) + 'Kio'
    elif n/1024/1024 < 1024:
        return str(Decimal(n/1024/1024).quantize(Decimal('.02'))) + 'Mio'
    elif n/1024/1024/1024 < 1024:
            return str(Decimal(n/1024/1024/1024).quantize(Decimal('.02'))) + 'Gio'
    else:
        return str(Decimal(n/1024/1024/1024/1024).quantize(Decimal('.02'))) + 'Tio'

def GetName(string):
    a = "find {} -printf '%f'"
    return str(subprocess.check_output(a.format(string), shell=True))

"""
Optional
"""

Audio = """
=== AUDIO {} ===

Titre  : {}
Codec  : {}
Débit  : {}
Canaux : {}
Langue : {}
"""

Subtitle = """
=== SOUS-TITRE {} ===
Langue : {}
Format : {}
"""

"""
Templates
"""

cache = open('./Templates/video.txt')
video = cache.read()
cache.close()

cache = open('./Templates/music.txt')
music = cache.read()
cache.close()

"""
Main Code
"""

#Getting arguments

arguments = {}

try:
    arguments["source"] = argv[[i for i,x in enumerate(argv) if x in ['-s', '--source']][0] + 1]

except:
    arguments["source"] = None

try:
    arguments["codec"] = argv[[i for i,x in enumerate(argv) if x in ['-c', '--codec']][0] + 1]

except:
    arguments["codec"] = None

try:
    arguments["audio-track"] = argv[[i for i,x in enumerate(argv) if x == '--audio-track'][0] + 1]

except:
    arguments["audio-track"] = None

try:
    arguments["output"] = argv[[i for i,x in enumerate(argv) if x in ['-o', '--output']][0] + 1]

except:
    arguments["output"] = './info.nfo'


try:
    arguments["subtitle-track"] = argv[[i for i,x in enumerate(argv) if x == '--subtitle-track'][0] + 1]

except:
    arguments["subtitle-track"] = None

try:
    arguments["input"] = argv[[i for i,x in enumerate(argv) if x in ['-i', '--input']][0] + 1]

except:
    arguments["input"] = None




if len(argv) < 3:
    print("Missing arguments")

elif argv[1] == 'video':
    if arguments["input"] == None:
        info                      = {}
        FilePath                  = argv[2]
        vid                       = cv2.VideoCapture(FilePath)

        info['name']              = GetName(FilePath.replace(" ", "\ "))[:-1][2:]
        info['size']              = FormatData(path.getsize(FilePath))
        info['duration']          = TimeFormat(floor(int(vid.get(cv2.CAP_PROP_FRAME_COUNT)) / int(vid.get(cv2.CAP_PROP_FPS))))
        if arguments["source"] != None:
            info['source']        = arguments['source']
        else:
            info['source']        = input('Source                : ')
        info['resolution']        = str(int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))) + 'x' + str(int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        info['format']            = FilePath[-3:]
        info['fps']               = int(vid.get(cv2.CAP_PROP_FPS))
        if arguments['codec'] != None:
            info['codec']         = arguments['codec']
        else:
            info['codec']         = input('Codec Vidéo           : ')
        info['audio']             = ''
        info['subtitle']          = ''
        if arguments['audio-track'] != None:
            i                     = arguments['audio-track']
        else:
            i                     = input('Nombre de piste audio : ')
        for x in range(int(i)):
            cache                 = {}
            cache['id']           = x + 1
            cache['title']        = input('Titre de la piste     : ')
            cache['codec']        = input('Codec de la piste     : ')
            cache['debit']        = input('Débit de la piste     : ')
            cache['canaux']       = input('Nombre de cannaux     : ')
            cache['language']     = input('Langue de la piste    : ')
            info['audio']        += Audio.format(cache['id'], cache['title'], cache['codec'], cache['debit'], cache['canaux'], cache['language'])
        if arguments["subtitle-track"] != None:
            i                     = arguments['subtitle-track']
        else:
            i                     = input('Nombre de sous-titre  : ')
        for x in range(int(i)):
            cache                 = {}
            cache['id']           = x + 1
            cache['language']     = input('Langue de la piste    : ')
            cache['format']       = input('Format de la piste    : ')
            info['subtitle']     += Subtitle.format(cache['id'], cache['language'], cache['format'])

        out     = video.format(info['name'], info['size'], info['duration'], info['source'], info['resolution'], info['format'], info['fps'], info['codec'], info['audio'], info['subtitle'])
        OutFile = open(arguments['output'], 'w')
        OutFile.write(out)
        OutFile.close()
    else:
        try:
            infoFile = open(arguments['input'], mode='r')
            ver = True
        except FileNotFoundError:
            print("Sorry but \"{}\" doesn't exist".format(arguments['input']))
        except PermissionError:
            print("You have not enought permission to open \"{}\"".format(arguments['input']))

        if ver == True:
            info  = infoFile.read().replace(' ', 'THISISJUSTAMOTHERFUCKINGSPACESOJUSTDONTUSETHISYNTAX').replace('\n', ' ').split()
            cache = []
            for i in info:
                i = i.replace('THISISJUSTAMOTHERFUCKINGSPACESOJUSTDONTUSETHISYNTAX', ' ')
                cache.append(i)
            info = cache
            FilePath                  = info[0]
            vid                       = cv2.VideoCapture(FilePath)
            info['name']              = info[1]
            info['size']              = FormatData(path.getsize(FilePath))
            info['duration']          = TimeFormat(floor(int(vid.get(cv2.CAP_PROP_FRAME_COUNT)) / int(vid.get(cv2.CAP_PROP_FPS))))
            info['source']            = info[3]
            info['resolution']        = str(int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))) + 'x' + str(int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT)))
            info['format']            = FilePath[-3:]
            info['fps']               = int(vid.get(cv2.CAP_PROP_FPS))
            info['codec']             = info[4]
            info['audio']             = ''
            info['subtitle']          = ''
            i                         = info[5]
            j                         = 5
            for x in range(int(i)):
                cache                 = {}
                cache['id']           = x + 1
                cache['title']        = info[j+1]
                cache['codec']        = info[j+2]
                cache['debit']        = info[j+3]
                cache['canaux']       = info[j+4]
                cache['language']     = info[j+5]
                info['audio']        += Audio.format(cache['id'], cache['title'], cache['codec'], cache['debit'], cache['canaux'], cache['language'])
                j                    += 5
            j                        += 1
            i                         = info[j]
            for x in range(int(i)):
                cache                 = {}
                cache['id']           = x + 1
                cache['language']     = info[j+1]
                cache['format']       = info[j+2]
                info['subtitle']     += Subtitle.format(cache['id'], cache['language'], cache['format'])
                j                    += 2

            out     = video.format(info['name'], info['size'], info['duration'], info['source'], info['resolution'], info['format'], info['fps'], info['codec'], info['audio'], info['subtitle'])
            OutFile = open(info[-1], 'w')
            OutFile.write(out)
            OutFile.close()
else:
    print("Unknow format")
